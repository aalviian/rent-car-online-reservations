<!DOCTYPE html>
<?php
	include "connect.php";
?>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facilities | Rent Cars</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<?php
	require "header.php";
?>		
	    		<div class="col-sm-12">    			   			
					<h2 class="title text-center">Our Facilities</h2>    			    				    				
				</div>			 		
            <center><h3><p>Ini fasilitas-fasilitas yang kami sediakan: </p></h3></center>
			<br>
			<br>
			<br>
				<div class="row facilities-list">
					<div class="col-sm-6 col-md-3 text-center service animated fadeInUp visible" data-animation="fadeInUp">
						<div class="service-icon"> 
                            <img src="images\fasilitas\supir-cartoon-car-taxi-wi.jpg" alt=""/> 
						</div>
						<h4 class="color-white">Supir</h4>
						<p>Tersedia supir jika membutuhkan (dikenakan biaya tambahan)</p>
					</div><!-- END COLUMN 3 -->
					<div class="col-sm-6 col-md-3 text-center service animated fadeInUp visible" data-animation="fadeInUp" data-animation-delay="200">
						<div class="service-icon">
							<img src="images\fasilitas\asuransi.jpg" alt=""/> 
						</div>
						<h4 class="color-white">Asuransi</h4>
						<p>Sebagai jaminan keselamatan</p>
					</div><!-- END COLUMN 3 -->
					<div class="col-sm-6 col-md-3 text-center service animated fadeInUp visible" data-animation="fadeInUp" data-animation-delay="400">
						<div class="service-icon">
							<img src="images\fasilitas\hand-etollcard.jpg" alt=""/> 
						</div>
						<h4 class="color-white">E-Toll Card</h4>
						<p>Untuk memudahkan akses Tol</p>
					</div><!-- END COLUMN 3 -->
					<div class="col-sm-6 col-md-3 text-center service animated fadeInUp visible" data-animation="fadeInUp" data-animation-delay="600">
						<div class="service-icon">
							<img src="images\fasilitas\IMG_0260.jpg" alt=""/> 
						</div>
						<h4 class="color-white">Joki</h4>
						<p>Dengan Joki yang kami sediakan, anda lebih nyaman</p>
					</div><!-- END COLUMN 3 -->
				</div>	


<?php	
	require "footer.php";
?>

  
    <script src="js/jquery.js"></script>
	<script src="js/price-range.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>
</html>
        