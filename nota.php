<?php 
include "connect.php";
require('fpdf/fpdf.php');
$pdf=new FPDF();
$pdf->AddPage();
							$queri1= mysqli_query($conn, "Select current_date as crrdate");
							$data3= mysqli_fetch_assoc($queri1);
							$date= $data3['crrdate'];

							$id=$_SESSION['id'];

							$max = mysqli_query($conn,"SELECT MAX(id_sewa) FROM pemesanan WHERE id_user = '$id'");
							$maxdata = mysqli_fetch_array($max);
							$id_transaksi = $maxdata[0];


							if($id_transaksi){
								$result = mysqli_query($conn, "SELECT  tanggal_hrs_kembali FROM pemesanan
									WHERE id_user = '$id' and id_sewa='$id_transaksi' "); 
								$row = mysqli_fetch_assoc( $result );
								$data2=$row['tanggal_hrs_kembali'];
								$query1 = mysqli_query($conn, "SELECT datediff('$data2', '$date')");
								$data1 = mysqli_fetch_array($query1);
								$interval = $data1[0];


								$query2 = mysqli_query($conn, "SELECT *
									FROM pemesanan pms INNER JOIN pengembalian pbl ON pbl.id_sewa = pms.id_sewa
									WHERE pms.id_user = '$id' and pms.id_sewa='$id_transaksi'");
									$data2 = mysqli_fetch_assoc($query2);
							
								if ($interval>=0 || !$data2['id_sewa'] ) {	?>	
							
						<?php
							$querynama = mysqli_query($conn,"SELECT nama_user FROM user WHERE id_user = '$id'");
							$datanama = mysqli_fetch_array($querynama);
							
							
							$result = mysqli_query($conn, "SELECT bio.nama_mobil, pms.tanggal_sewa, pms.tanggal_hrs_kembali, pms.biaya FROM biodata_mobil bio 
												INNER JOIN pemesanan pms ON pms.id_mobil = bio.id_mobil where pms.id_sewa='$id_transaksi'");
							while ($data= mysqli_fetch_assoc($result)) {
$pdf->Cell(43,10,'');
$pdf->SetFont('Arial','B',32);
$pdf->Cell(120,10,'ROYAL RENT CAR');
$pdf->SetFont('Arial','',12);	
$pdf->Ln();
$pdf->Cell(70,10,'');
$pdf->Cell(20,10,'ID User: ');
$pdf->Cell(50,10,$id);
$pdf->Ln();
$pdf->Ln();
$pdf->Ln();
$pdf->SetFont('Arial','',18);
$pdf->Cell(50,10,'Anda telah menyewa: ');
$pdf->Ln();
$pdf->SetFont('Arial','',10);
$pdf->Cell(80,10,'--------------------------------------------------------------------------------------------------------------------------');
$pdf->SetFont('Arial','',18);	
$pdf->Ln();
$pdf->Cell(80,10,'Nama Mobil                     : ');
$pdf->SetFont('Arial','B',22);											
$pdf->Cell(50,10,$data['nama_mobil']);
$pdf->Ln();
$pdf->SetFont('Arial','',18);	
$pdf->Cell(80,10,'Tanggal Sewa                 : ');
$pdf->SetFont('Arial','B',22);
$pdf->Cell(50,10,$data['tanggal_sewa']);
$pdf->Ln();
$pdf->SetFont('Arial','',18);	
$pdf->Cell(80,10,'Tanggal Pengembalian   : ');
$pdf->SetFont('Arial','B',22);
$pdf->Cell(50,10,$data['tanggal_hrs_kembali']);
$pdf->Ln();
$pdf->SetFont('Arial','',18);	
$pdf->Cell(80,10,'Biaya(Mobil+Fasilitas)     : ');
$pdf->SetFont('Arial','B',22 );
$pdf->Cell(50,10,'Rp '.number_format($data['biaya'],0));
$pdf->Ln();
$pdf->SetFont('Arial','',10);
$pdf->Cell(80,10,'--------------------------------------------------------------------------------------------------------------------------');
$pdf->Ln();
$pdf->Ln();
$pdf->Ln();
$pdf->Cell(10,10,'');
$pdf->SetFont('Arial','',14);	
$pdf->Cell(120,10,'Tanda Tangan');
$pdf->Cell(10,10,'');
$pdf->Cell(80,10,'Tanda Tangan');
$pdf->Ln();
$pdf->Cell(9,10,'');
$pdf->Cell(120,10,'Kepala Cabang');
$pdf->SetFont('Arial','',14);	
$pdf->Cell(16,10,'');
$pdf->Cell(120,10,'Penyewa');
$pdf->Ln();
$pdf->Ln();
$pdf->Ln();
$pdf->SetFont('Arial','',10);
$pdf->Cell(130,10,'___________________________');
$pdf->SetFont('Arial','',10);
$pdf->Cell(80,10,'___________________________');
$pdf->Ln();
$pdf->Cell(153,10,'');
$pdf->Cell(80,10,$datanama['nama_user']);
$pdf->Ln();
$pdf->Ln();
$pdf->SetFont('Arial','',10);
$pdf->Cell(80,10,'Catatan:');	
$pdf->Ln();
$pdf->Cell(80,10,'Silahkan cetak dan bawa nota ini saat pengambilan mobil sewa di toko sebagai tanda bukti');
$pdf->Ln();
$pdf->Ln();
$pdf->Ln();
$pdf->Cell(4,10,'');
$pdf->SetFont('Arial','',20);
$pdf->Cell(80,10,'~TERIMA KASIH SUDAH MENGGUNAKAN JASA KAMI~');	
}}}
$pdf->Output();
?>
