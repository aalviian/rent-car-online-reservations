<!DOCTYPE html>
<?php
	include 'connect.php';

	if($_SESSION['id']=="kucing"){
	header("location:index.php");
	}
	
?>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Account | Rent Car</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->
<body>
	<?php
		require 'header.php';
	?>
	<style>
.CSSTableGenerator {

 margin:0px;padding:0px;

 width:100%;
 box-shadow: 10px 10px 5px #888888;

 border:1px solid #bf0000;

 

 -moz-border-radius-bottomleft:14px;

 -webkit-border-bottom-left-radius:14px;

 border-bottom-left-radius:14px;

 

 -moz-border-radius-bottomright:14px;

 -webkit-border-bottom-right-radius:14px;

 border-bottom-right-radius:14px;

 

 -moz-border-radius-topright:14px;

 -webkit-border-top-right-radius:14px;

 border-top-right-radius:14px;

 

 -moz-border-radius-topleft:14px;

 -webkit-border-top-left-radius:14px;

 border-top-left-radius:14px;

}.CSSTableGenerator table{

 width:100%;

 height:100%;

 margin:0px;padding:0px;

}.CSSTableGenerator tr:last-child td:last-child {

 -moz-border-radius-bottomright:14px;

 -webkit-border-bottom-right-radius:14px;

 border-bottom-right-radius:14px;

}

.CSSTableGenerator table tr:first-child td:first-child {

 -moz-border-radius-topleft:14px;

 -webkit-border-top-left-radius:14px;

 border-top-left-radius:14px;

}

.CSSTableGenerator table tr:first-child td:last-child {

 -moz-border-radius-topright:14px;

 -webkit-border-top-right-radius:14px;

 border-top-right-radius:14px;

}.CSSTableGenerator tr:last-child td:first-child{

 -moz-border-radius-bottomleft:14px;

 -webkit-border-bottom-left-radius:14px;

 border-bottom-left-radius:14px;

}.CSSTableGenerator tr:hover td{

 background-color:#ffaaaa;

  


}

.CSSTableGenerator td{

 vertical-align:middle;

 

 background-color:#ffffff;


 border:1px solid #bf0000;

 border-width:0px 1px 1px 0px;

 text-align:left;

 padding:7px;

 font-size:15px;

 font-family:Arial;

 font-weight:normal;

 color:#000000;

}.CSSTableGenerator tr:last-child td{

 border-width:0px 1px 0px 0px;

}.CSSTableGenerator tr td:last-child{

 border-width:0px 0px 1px 0px;

}.CSSTableGenerator tr:last-child td:last-child{

 border-width:0px 0px 0px 0px;

}

.CSSTableGenerator tr:first-child td{

  background:-o-linear-gradient(bottom, #ff5656 5%, #ff5656 100%); background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #ff5656), color-stop(1, #ff5656) );
 background:-moz-linear-gradient( center top, #ff5656 5%, #ff5656 100% );
 filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#ff5656", endColorstr="#ff5656"); background: -o-linear-gradient(top,#ff5656,ff5656);


 background-color:#ff5656;

 border:0px solid #bf0000;

 text-align:center;

 border-width:0px 0px 1px 1px;

 font-size:15px;

 font-family:Arial;

 font-weight:bold;

 color:#000000;

}

.CSSTableGenerator tr:first-child:hover td{

 background:-o-linear-gradient(bottom, #ff5656 5%, #ff5656 100%); background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #ff5656), color-stop(1, #ff5656) );
 background:-moz-linear-gradient( center top, #ff5656 5%, #ff5656 100% );
 filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#ff5656", endColorstr="#ff5656"); background: -o-linear-gradient(top,#ff5656,ff5656);


 background-color:#ff5656;

}

.CSSTableGenerator tr:first-child td:first-child{

 border-width:0px 0px 1px 0px;

}

.CSSTableGenerator tr:first-child td:last-child{

 border-width:0px 0px 1px 1px;

}
</style>
	 <div id="contact-page" class="container">
    	<div class="bg">
			<div class="row">    		
	    		<div class="col-sm-12">    			   			
					<h2 class="title text-center">My Account</h2>    			    				    				
				</div>			 		
			</div>    
		<div class="row">  	
		<?php
			if(isset($_SESSION['id'])) {
			$id = $_SESSION['id'];
			$query = mysqli_query($conn, "SELECT * FROM user WHERE id_user = '$id'");
			$result = mysqli_fetch_array($query);
		?>
			<div class="col-sm-8">
			  <div class="contact-info">
				<div class="CSSTableGenerator">
				<table border="table table-bordered">
					  <tr>
					  <div class="col-sm-0">
					  <img src="<?php echo $mobil['foto'] ?>" width="220" height="165"/>
					  </div>
					  </tr>
					  <tr style="background-color:#33ff99">
					  <th>DATA DIRI</th>
					  <th>ADALAH</th>
					  </tr>
					  <tr>
					  <td>Name</td><td style="text-align:left"><?php echo $result['nama_user']?></td>
					  </tr>
					  <tr>
					  <td>PIN</td><td style="text-align:left"><?php echo $result['no.ktp']?></td>
					  </tr>
					  <tr>
					  <td>Address</td><td style="text-align:left"><?php echo $result['alamat']?></td>
					  </tr>
					  <tr>
					  <td>Date of Birth</td><td style="text-align:left"><?php echo $result['tgllahir']?></td>
					  </tr>
					  <tr>
					  <td>Email</td><td style="text-align:left"><?php echo $result['email']?></td>
					  </tr>
					  <tr>
					  <td>Handphone</td><td style="text-align:left"><?php echo $result['no.hp']?></td>
					  </tr>
					  <tr style="text-align:right;background-color:#FFCB68;font-weight:bold">
					  </tr>
				</table>
				</div>
			 </div>
			</diV>
				<?php } ?>
			<div class="col-sm-4">
					<div class="contact-info">
					</div>
			 </div>
		</div>
		</div>
	</div>
	<br>
	<br>
	<br>
	<br>
	<br>
	<?php
		require 'footer.php';
	?>
    <script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/price-range.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>
</html>