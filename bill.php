<?php
	include "connect.php";

	if($_SESSION['id']=="kucing"){
	header("location:index.php");
	}

?>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Bill | Rent Car</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
	<?php
		require 'header.php';
	?>
		<div id="contact-page" class="container">
		<div class="bg">
	    	<div class="row">    		
	    		<div class="col-sm-12">		
					<center><h2 class="title text-center">Pembayaran</h2></center>
					<div class="box-body">
						<table class="table table-bordered">
				                <tr>
				                 <th >Nama Pemesan</th>
						    	 <th>Nama Mobil</th>
				                 <th>Tanggal Sewa</th>
				                 <th>Tanggal Kembali</th>
				                 <th>Biaya</th>
								</tr>
					<?php $queri1= mysqli_query($conn, "Select current_date as crrdate");
							$data3= mysqli_fetch_assoc($queri1);
							$date= $data3['crrdate'];

							$id=$_SESSION['id'];

							$max = mysqli_query($conn,"SELECT MAX(id_sewa) FROM pemesanan WHERE id_user = '$id'");
							$maxdata = mysqli_fetch_array($max);
							$id_transaksi = $maxdata[0];


							if($id_transaksi){
								$result = mysqli_query($conn, "SELECT  tanggal_hrs_kembali FROM pemesanan
									WHERE id_user = '$id' and id_sewa='$id_transaksi' "); 
								$row = mysqli_fetch_assoc( $result );
								$data2=$row['tanggal_hrs_kembali'];
								$query1 = mysqli_query($conn, "SELECT datediff('$data2', '$date')");
								$data1 = mysqli_fetch_array($query1);
								$interval = $data1[0];


								$query2 = mysqli_query($conn, "SELECT *
									FROM pemesanan pms INNER JOIN pengembalian pbl ON pbl.id_sewa = pms.id_sewa
									WHERE pms.id_user = '$id' and pms.id_sewa='$id_transaksi'");
									$data2 = mysqli_fetch_assoc($query2);
							
								if ($interval>0 || !$data2['id_sewa'] ) {	?>	
							
						<?php
							$querynama = mysqli_query($conn,"SELECT nama_user FROM user WHERE id_user = '$id'");
							$datanama = mysqli_fetch_array($querynama);
							
							
							$result = mysqli_query($conn, "SELECT bio.nama_mobil, pms.tanggal_sewa, pms.tanggal_hrs_kembali, pms.biaya FROM biodata_mobil bio 
												INNER JOIN pemesanan pms ON pms.id_mobil = bio.id_mobil where pms.id_sewa='$id_transaksi'");
							while ($data= mysqli_fetch_assoc($result)) {
						?>					
							
								<tr>
								 <td><?php echo $datanama[0]?></td>
				                 <td><?php echo $data['nama_mobil']?></td>
								 <td><?php echo $data['tanggal_sewa']?></td>
				                 <td><?php echo $data['tanggal_hrs_kembali']?></td>
				                 <td><?php echo "Rp ".number_format($data['biaya'],0)?></td>
								</tr>
						</table>

								<a href="cancelsewa.php?id_sewa=<?php echo $id_transaksi?>">Batalkan Pesanan</a>
								<br>
								<a href="nota.php">Nota Pesanan</a>
							<?php }}
							else {?> </table> <?php echo "Tidak ada pesanan"?> <?php }
							} else {?> </table> <?php echo "Tidak ada pesanan"; }?>
					</div>
			
					*) Tagihan diatas belum termasuk denda keterlambatan pengembalian					
				</div>			 		
			</div>	
		</div>
		</div>
	<?php	
		require 'footer.php';
	?>
    <script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/price-range.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>
</html>