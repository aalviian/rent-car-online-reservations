<!DOCTYPE html>
<?php
	include "connect.php";
	
	if($_SESSION['id']=="kucing"){
	header("location:index.php");
	}
	
	$queri1= mysqli_query($conn, "Select current_date as crrdate");
	$data= mysqli_fetch_assoc($queri1);
	$date= $data['crrdate'];

	$id=$_SESSION['id'];

	$max = mysqli_query($conn,"SELECT MAX(id_sewa) FROM pemesanan WHERE id_user = '$id'");
	$maxdata = mysqli_fetch_array($max);
	$id_transaksi = $maxdata[0];
	
	if($id_transaksi){
		$result = mysqli_query($conn, "SELECT  tanggal_hrs_kembali FROM pemesanan
							  WHERE id_user = '$id' and id_sewa='$id_transaksi' "); 
		$row = mysqli_fetch_assoc( $result );
		$data2=$row['tanggal_hrs_kembali'];
		$query1 = mysqli_query($conn, "SELECT datediff('$data2', '$date')");
		$data1 = mysqli_fetch_array($query1);
		$interval = $data1[0];

		if ($interval>0){ ?>
			<script language="javascript">alert("Silahkan bayar atau batalkan pemesanan terlebih dulu");</script>
			<script>document.location.href='index.php';</script>
		<?php }

		else if ($interval<=0){ 
			$query2 = mysqli_query($conn, "SELECT * FROM pemesanan pms INNER JOIN pengembalian pbl ON pbl.id_sewa = pms.id_sewa
								WHERE pms.id_user = '$id' and pms.id_sewa=$id_transaksi ");
			$data2 = mysqli_fetch_assoc($query2);
				if (!$data2['biaya']){ ?>
					<script language="javascript">alert("Silahkan bayar atau batalkan pemesanan terlebih dulu");</script>
					<script>document.location.href='index.php';</script>
		<?php 
				}
		}		
	}
?>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Rent | Rent Cars</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<?php
	require "header.php";
?>
<?php
	if(!isset($_GET['idmobil']))
	header("location:cars.php");
	$idmobil = $_GET['idmobil'];
	$sql = mysqli_query($conn, "SELECT nama_mobil, foto FROM biodata_mobil where id_mobil='$idmobil'");
	$mobil = mysqli_fetch_assoc($sql);
?>
	 <div id="contact-page" class="container">
    	<div class="bg">
	    	<div class="row">    		
	    		<div class="col-sm-12">		
					<center><h2 class="title text-center">Penyewaan </h2></center>    			    				    				
				</div>			 		
			</div>
			
			<p><H2>Anda menyewa mobil "<?php echo $mobil['nama_mobil']?>"</H2></p>
			
	    	<div class="row">
				<div class="signup-form">
					<form action="sewapost.php" method="POST">
						<div class="col-sm-6">
							Tanggal Peminjaman :
							<input type="date" name="mulai" placeholder="Tanggal Peminjaman" required />
							Tanggal Pengembalian :
							<input type="date" name="kembali" placeholder="Tanggal Pengembalian" required />
							<input type="hidden" name="idmobil" value="<?php echo $idmobil?>" required />
						</div>	
				</div>
						<div class="col-sm-6">  
					<div class="chose_area">
						<ul class="user_option">
							<h4>Fasilitas (*dikenakan biaya tambahan</h4>
							<li><label>
								<input type="checkbox" name="joki" value="200000">
								Jockey (Rp.200.000/day)</label>
							</li>
							<li><label>
								<input type="checkbox" name="supir" value="100000">
								Driver (Rp.100.000/day)</label>
							</li>
							<li><label>
								<input type="checkbox" name="asuransi" value="10000">
								Assurance (Rp.10.000)</label>
							</li>
							<li><label>
								<input type="checkbox" name="etoll" value="50000">
								E-Toll (Rp.50.000)</label>
							</li>							
						</ul>
					</div>
						</div>
			<div class = "row">
				<div class="col-sm-6">
				<div class="pull-right">
				<input type="submit" class="btn btn-default"  action="sewapost.php" method="POST" value="Agree">
				</div>
				</div>
			</div>	
					</form>
			</div> 
    	</div>	
		<div class="row">
			<p>* All prices above include payment taxes</p>
		</div>
    </div>
	
<?php	
	require "footer.php";
?>
  
    <script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
    <script type="text/javascript" src="js/gmaps.js"></script>
	<script src="js/contact.js"></script>
	<script src="js/price-range.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>
</html>