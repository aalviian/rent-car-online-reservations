<!DOCTYPE html>
<?php
	include 'connect.php';	

?>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Home | Rent Car</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
	<?php
		require 'header.php';
 
			$query1=mysqli_query($conn, "SELECT DISTINCT kategori FROM biodata_mobil");
			while ($kategori=mysqli_fetch_assoc($query1)) {
			
	?>
<div class="col-sm-12 padding-right">
	<div class="features_items"><!--features_items-->
		<h2 class="title text-center"><?php echo $kategori['kategori'] ?> Cars</h2>		
			<?php
				$data =$kategori['kategori'];
				$qry = mysqli_query($conn,"SELECT * FROM biodata_mobil WHERE kategori='$data' ");
				while ($mobil = mysqli_fetch_assoc($qry)) {
			?>
				<div class="col-sm-3">
					<div class="product-image-wrapper">
						<div class="single-products">
							<div class="productinfo text-center">
								<img src="<?php echo $mobil['foto']?>" alt="" />
								<h2>Rp<?php echo $mobil['harga']?>/day</h2>
								<p><?php echo $mobil['nama_mobil']?></p>
								<?php if($mobil['stok']<=0) { ?>
									<a href="#.php?idmobil=<?php echo "Full Booked"?>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Sewa</a>
								<?php } else { ?>
									<a href="sewa.php?idmobil=<?php echo $mobil['id_mobil']?>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Sewa</a>
								<?php } ?>
							</div>
							<div class="product-overlay">
								<div class="overlay-content">
									<?php if(($mobil['stok']<=0)) { ?>
										<p><h2> Full Booked </h2></p>
									<?php } else { ?>
										<p>Ada <?php echo $mobil['stok']?> Stok<p>
									<?php } ?>
										<h2>Rp<?php echo $mobil['harga']?>/day</h2>
										<p><?php echo $mobil['nama_mobil']?></p>
									<?php if($mobil['stok']==0) { ?>
										<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Sewa</a>
									<?php } 
										else {
											if(!(isset($_SESSION['id']))) { ?>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Sewa</a>														
											<?php }	
											else { ?>
												<a href="sewa.php?idmobil=<?php echo $mobil['id_mobil']?>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Sewa</a>													
											<?php } 
										} ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php }  ?>					
	</div><!--features_items-->
</div>
<?php } ?>
	<?php
		require 'footer.php';
	?>
    <script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/price-range.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>
</html>