<!DOCTYPE html>
<?php
	include "connect.php";

?>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Registration | Rent Car</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
	
	<?php	
		require "header.php";
	?>
	<div class="col-sm-12">    			   			
		<h2 class="title text-center">Registration Form</h2>    			    				    				
	</div>	
	<section id="form"><!--form-->
		<div class="container  text-center service animated fadeInUp visible">
			<div class="row">
				<div class="col-sm-4"></div>
				<h4>Don't have an account? Please register here</h4>
				<div class="col-sm-4"></div>
				<div class="col-sm-4">
				<div class="pull-left">
					<div class="signup-form"><!--sign up form-->
						<h2>New User Signup!</h2>
						<form action="signup.php" method="post">
							<div class="form-group">
								<label>Full Name:</label>
									<div class="input-group">
										<div class="input-group-addon">
											<i class="fa fa-user"></i>
										</div>
										<input type="text" class="form-control" name="nama" required />
									</div><!-- /.input group -->
							</div><!-- /.form group -->
							<div class="signup-form">
								<label>Personal Identity Number:</label>
									<div class="input-group">
										<div class="input-group-addon">
											<i class="fa fa-file-text-o"></i>
										</div>
										<input type="text" class="form-control" maxlength="12" name="noktp" required />
									</div><!-- /.input group -->
							</div><!-- /.form group -->
							<div class="form-group">
								<label>Address:</label>
									<div class="input-group">
										<div class="input-group-addon">
											<i class="fa fa-envelope"></i>
										</div>
										<input type="text" class="form-control" name="alamat" placeholder="Alamat Lengkap" required />
									</div><!-- /.input group -->
							</div><!-- /.form group -->
							<div class="signup-form">
								<label>Date of Birth:</label>
									<div class="input-group">
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
										<input type="date" name="tanggal" required" />
									</div><!-- /.input group -->
							</div><!-- /.form group -->
							<div class="form-group">
								<label>Handphone Number:</label>
									<div class="input-group">
										<div class="input-group-addon">
											<i class="fa fa-phone"></i>
										</div>
										<input type="text" class="form-control" maxlength="12" name="telepon" required />
									</div><!-- /.input group -->
							</div><!-- /.form group -->
							<div class="form-group">
								<label>Email:</label>
									<div class="input-group">
										<div class="input-group-addon">
											<i class="fa ">@</i>
										</div>
										<input type="email" class="form-control" name="email" required />
									</div><!-- /.input group -->
							</div><!-- /.form group -->
							<div class="form-group">
								<label>Password:</label>
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-lock"></i></span>
										<input type="password" class="form-control" name="password" placeholder="Password" required />
									</div>
							</div>
							<div class="signup-form">
								<label>Photo:</label>
									<div class="input-group">
										<div class="input-group-addon">
											<i class="fa fa-user"></i>
										</div>
										<input type="file" class="form-control" name="foto" required />
									</div><!-- /.input group -->
							</div><!-- /.form group -->
							<center><button type="submit" class="btn btn-default">Signup</button></center>
						</form>
					</div><!--/sign up form-->
				</div>
				</div>
			</div>
		</div>
	</section><!--/form-->
	<?php
		require 'footer.php';
	?>  
    <script src="js/jquery.js"></script>
	<script src="js/price-range.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>
</html>