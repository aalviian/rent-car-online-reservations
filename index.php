<!DOCTYPE html>
<?php
	include 'connect.php';	

?>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Home | Rent Car</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
	<?php
		require 'header.php';
	?>
	<style>.ig-b- { display: inline-block; }
.ig-b- img { visibility: hidden; }
.ig-b-:hover { background-position: 0 -60px; } .ig-b-:active { background-position: 0 -120px; }
.ig-b-v-24 { width: 137px; height: 24px; background: url(//badges.instagram.com/static/images/ig-badge-view-sprite-24.png) no-repeat 0 0; }
@media only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min--moz-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2 / 1), only screen and (min-device-pixel-ratio: 2), only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx) {
.ig-b-v-24 { background-image: url(//badges.instagram.com/static/images/ig-badge-view-sprite-24@2x.png); background-size: 160px 178px; } }</style>
<a href="http://instagram.com/aalviian?ref=badge" class="ig-b- ig-b-v-24"><img src="//badges.instagram.com/static/images/ig-badge-view-24.png" alt="Instagram" /></a>
	<section id="slider"><!--slider-->
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div id="slider-carousel" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
							<li data-target="#slider-carousel" data-slide-to="1"></li>
							<li data-target="#slider-carousel" data-slide-to="2"></li>
						</ol>						
						<div class="carousel-inner">
							<div class="item active">
							<?php 
								$query2 = mysqli_query($conn, "SELECT * FROM biodata_mobil ORDER BY id_mobil DESC LIMIT 1");
								$pop=mysqli_fetch_assoc($query2) ?>
								<div class="col-sm-6">
								
									<h1>ROYAL RENT CARS</h1>
									<h2>Mobil Ter-update !</h2>
									<p><?php echo $pop['nama_mobil']?></p>
									<a href="sewa.php?idmobil=<?php echo $pop['id_mobil']?>"class="btn btn-default get">Sewa </a>
								
								</div>
								<div class="col-sm-6">
									<img src="<?php echo $pop['foto']?> " class="girl img-responsive" alt="" />
								</div>	
							</div>
							<?php 
									$query3 = mysqli_query($conn, "SELECT * FROM biodata_mobil ORDER BY id_mobil DESC LIMIT 3");
									while($pop2=mysqli_fetch_assoc($query3)) { if($pop2['id_mobil']<$pop['id_mobil']) { ?>
							<div class="item">
								<div class="col-sm-6">
									<h1>RENT CARS</h1>
									<h2>Mobil Ter-update !</h2>
									<p><?php echo $pop2['nama_mobil'] ?> </p>
									<a type="button" href="sewa.php?idmobil=<?php echo $pop2['id_mobil']?>" class="btn btn-default get"> Sewa</a>
								</div>
								<div class="col-sm-6">
									<img src="<?php echo $pop2['foto']?>" class="girl img-responsive" alt="" />
								</div>
							</div>
							<?php } } ?>
						</div>
							
						<a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
							<i class="fa fa-angle-left"></i>
						</a>
						<a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
							<i class="fa fa-angle-right"></i>
						</a>
					</div>
					
				</div>
			</div>
		</div>
	</section><!--/slider-->
	
	<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="left-sidebar">
						<h2>Category</h2>
						<div class="panel-group category-products" id="accordian"><!--category-productsr-->
							<?php 
								$query1=mysqli_query($conn, "SELECT DISTINCT kategori FROM biodata_mobil");
								while ($kategori=mysqli_fetch_assoc($query1)) {
							?>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordian" href="#<?php echo $kategori['kategori'] ?>">
											<span class="badge pull-right"><i class="fa fa-plus"></i></span>
											<?php echo $kategori['kategori'] ?>
										</a>
											
									</h4>
								</div>
								<div id="<?php echo $kategori['kategori'] ?>" class="panel-collapse collapse">
									<div class="panel-body">
										<ul>
											<?php 
												$temp=$kategori['kategori'];
												$query2 = mysqli_query($conn,"SELECT nama_mobil, id_mobil FROM biodata_mobil WHERE kategori='$temp' " );
												while ($mobil2=mysqli_fetch_assoc($query2)){
											?>
											<li><a href="sewa.php?idmobil=<?php echo $mobil2['id_mobil']?>"> <?php echo $mobil2['nama_mobil']?> </a></li>
											<?php } ?>
										</ul>
									</div>
								</div>
							</div>
							<?php } ?>
						</div><!--/category-products-->
					</div>
				</div>
									<div class="col-sm-9 padding-right">
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center">Cars Items</h2>
						
						<?php
							$qry = mysqli_query($conn,"SELECT * FROM biodata_mobil");
							while ($mobil = mysqli_fetch_assoc($qry)) {
						?>
						<div class="col-sm-4">
							<div class="product-image-wrapper">
								<div class="single-products">
										<div class="productinfo text-center">
											 <img src="<?php echo $mobil['foto']; ?>"
											<h2><?php echo"Rp ".number_format($mobil['harga'],0) ?>/hari</h2>
											<p><?php echo $mobil['nama_mobil']?></p>
													<?php
													if($mobil['stok'] <= 0) { ?>
														<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Sewa</a>
													<?php } 
													else {?>
													<a href="sewa.php?idmobil=<?php echo $mobil['id_mobil']?>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Sewa</a>
													<?php } ?>
										</div>
										<div class="product-overlay">
											<div class="overlay-content">
												<?php
													if($mobil['stok'] == 0) { ?>
														<p><h2>Full Booked</h2></p>
													<?php } else { ?>
														<p>Sedia <?php echo $mobil['stok']?> Stok</p>
													<?php }
												?>
												<h2><?php echo"Rp ".number_format($mobil['harga'],0) ?>/hari</h2>
												<p><?php echo $mobil['nama_mobil']?></p>
													<?php
													if($mobil['stok'] <= 0) { ?>
														<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Sewa</a>
													<?php } 
													else {?>
													<a href="sewa.php?idmobil=<?php echo $mobil['id_mobil']?>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Sewa</a>
													<?php } ?>
											</div>
										</div>
								</div>
							</div>
						</div>
						<?php } ?>
					
						
						<ul class="pagination">
							<li class="active"><a href="">1</a></li>
							<li><a href="">2</a></li>
							<li><a href="">3</a></li>
							<li><a href="">&raquo;</a></li>
						</ul>
					</div><!--features_items-->
				</div>
			</div>
		</div>
	</section>
	<?php
		require 'footer.php';
	?>
    <script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/price-range.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>
</html>