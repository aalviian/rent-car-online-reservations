	<footer id="footer"><!--Footer-->
		<div class="footer-widget">
			<div class="container">
				<div class="row">
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>Address</h2>
								<p>Lingkar Kampus Dalam<br>
								Jalan Raya Dramaga<br>
								16680, Kabupaten Bogor<br>
								Bogor, Jawab Barat, Indonesia</p><br>
								
						</div>
					</div>
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>Category</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="#">Sport</a></li>
								<li><a href="#">Jeep</a></li>
								<li><a href="#">Sedan</a></li>
								<li><a href="#">Coupe</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>Policies</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="#">Terms of Use</a></li>
								<li><a href="#">Privecy Policy</a></li>
								<li><a href="#">Refund Policy</a></li>
								<li><a href="#">Billing System</a></li>
								<li><a href="#">Rent System</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>About Royal</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="#">Royal Information</a></li>
								<li><a href="#">Careers</a></li>
								<li><a href="#">Store Location</a></li>
								<li><a href="#">Affillate Program</a></li>
								<li><a href="#">Copyright</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3 col-sm-offset-1">
						<div class="single-widget">
							<h2></h2>
							<form action="#" class="searchform">
								<input type="text" placeholder="Your email address" />
								<button type="submit" class="btn btn-default"><i class="fa fa-arrow-circle-o-right"></i></button>
								<p>Royan Ajmal Alvian <br />our site and be updated your self...</p>
							</form>
						</div>
					</div>
					
				</div>
			</div>
		</div>
		
		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<p class="pull-left">Copyright © 2013 Royal Rent Cars Inc. All rights reserved.</p>
					<p class="pull-right">Designed by <span><a target="_blank" href="http://royalrentcar.hol.es/royal">Royal Project</a></span></p>
				</div>
			</div>
		</div>
		
	</footer><!--/Footer-->