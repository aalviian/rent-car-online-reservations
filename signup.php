<?php
	include "connect.php";
?>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Contact | Rent Car</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
<header id="header"><!--header-->
		<div class="header-bottom"><!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="index.php">Home</a></li>
								<?php
								if($_SESSION['id']=="kucing") { ?>

                                    <li><a href="cars.php">Cars</a></li>							
									<li><a href="rentrules.php">Rent Rules</a></li>
									<li><a href="contact-us.php">Contact</a></li>
									<li><a href="facilities.php">Facilities</a></li> 								
								<?php }
								
								if($_SESSION['id']!="kucing") {
								$user = $_SESSION['id'];
								$queri_bla = mysqli_query($conn, "SELECT level from user where id_user='$user'");
								while($row = mysqli_fetch_assoc( $queri_bla )) {
									$level = $row['level'];
								}
								if(!$level) { 
									?>
                                    <li><a href="cars.php">Cars</a></li>						
									<li><a href="rentrules.php">Rent Rules</a></li>
									<li><a href="contact-us.php">Contact</a></li>
									<li><a href="facilities.php">Facilities</a></li><?php
									}
								else { ?>
									<li><a href="mobil.php">Cars Input</a></li>
									<li><a href="daftaruser.php">Users List</a></li>
									<li><a href="daftarkomentar.php">Comments List</a></li>
									<li><a href="checkout.php"><i class="fa fa-crosshairs"></i> Reversion</a></li>
								<?php } ?>
								<?php } ?>
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
					
					</div>
				</div>
			</div>
		</div><!--/header-bottom-->
</header>

 <div id="contact-page" class="container">
    	<div class="bg">
	    	<div class="row">    		
	    		<div class="col-sm-12">    			   			
					<h2 class="title text-center">Email Verification</strong></h2>    			    				    				
				</div>			 		
			</div>
			
			<div class="row">  	
	    		<div class="col-sm-8">
	    			<div class="contact-form">
	    				<div class="status alert alert-success" style="display: none"></div>
<?php
	$folder =  "profil";
	$nama = $_POST['nama'];
	$ktp = $_POST['noktp'];
	$alamat = $_POST['alamat'];
	$tanggal = $_POST['tanggal'];
	$no_hp = $_POST['telepon'];
	$email = $_POST['email'];
	$pass = $_POST['password'];
	$password = md5($pass);
	$hash = md5(uniqid(rand(), true));
	$fotoo = $_FILES["foto"]["tmp_name"];
	$name = $folder."/".$_FILES["foto"]["name"];

	$sql1 = "SELECT * FROM user WHERE email = '$email'";
	$result1 = mysqli_query($mysqli,$sql1) or die(mysqli_error());
	if (mysqli_num_rows($result1) > 0)
            {
    $_SESSION['error']['email'] = "This Email is already used.";
	}
	else {
	move_uploaded_file($fotoo, $name);
	
	$result=mysqli_query($conn, "INSERT INTO `user`(`id_user`, `nama_user`,`no.ktp`,`alamat`,`tgllahir`,`no.hp`, `email`, `password`,`hash`, `foto`, `level`) 
								VALUES ('','$nama','$ktp','$alamat','$tanggal','$no_hp','$email','$pass','$hash','$fotooo', '0')");
	$id = mysqli_insert_id($conn);
if($result) {
   $to = $email;
   $subject = "Confirmation from Royal Rent Car to $nama";
   $header = "Royal: Confirmation from Royal";
   $message = "Please click the link below to verify and activate your account";
   $message = "www.royalrentcar.hol.es/verify.php?passkey=$hash";

   $sentmail = mail($to,$subject,$message,$header);

   if($sentmail) {
   echo "Your Confirmation link Has Been Sent To Your Email Address. Please check your email, and verify now!";
   }
   else {
    echo "Cannot send Confirmation link to your e-mail address";
   }
  }
  }
	mysqli_close($conn);
?>
					</div>
	    			</div>
			</div>
		</div>
	</div>
<br>
<br>
	<?php
		require 'footer.php';
	?>
    <script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
    <script type="text/javascript" src="js/gmaps.js"></script>
	<script src="js/contact.js"></script>
	<script src="js/price-range.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>
</html>