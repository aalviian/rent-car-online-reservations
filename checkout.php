<?php
	include "connect.php";
	$user = $_SESSION['id'];
	$queri_bla = mysqli_query($conn, "SELECT level from user where id_user='$user'");
	while($row = mysqli_fetch_assoc( $queri_bla )) {
		$level = $row['level'];
		
	}
	if(!($level))
		{
		header("location:index.php");
		}
?>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Reversion | Rent Car</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
	
	<?php
		require "header.php"
	?>
	
	<div id="contact-page" class="container">
    	<div class="bg">
	    	<div class="row">    		
	    		<div class="col-sm-12">		
					<center><h2 class="title text-center">Pengembalian</h2></center>    			    				    				
				</div>			 		
			</div>
	    	<div class="row">
				<div class="signup-form">
					<form action="checkoutpost.php" method="POST">
						<div class="col-sm-6">  
							Nama User  :
							<select name="nama">
									<?php $result1 = mysqli_query($conn, "SELECT DISTINCT (us.nama_user)
														FROM user us INNER JOIN pemesanan pms WHERE pms.id_user = us.id_user ORDER BY pms.id_sewa");
								while ($data = mysqli_fetch_assoc($result1)) {
								?>
								  <option name="nama" value="<?php echo $data['nama_user']?>"><?php echo $data['nama_user']?></option>
								<?php } ?>
							</select>
							<br>
							<br>
							<div class = "row">
								<div class="col-sm-6">
									<div class="pull-left">
										<input type="submit" class="btn btn-default"  action="checkoutpost.php" method="POST" value="Agree">
									</div>
								</div>
							</div>
						</div>
					</form>
				</div> 
				<table border="table table-bordered">
							<tr>
								<th style="width: 10px">No</th>
								<th>Tanggal Kembali</th>
								<th>Denda</th>
								<th>Biaya Total</th>
								<th>ID Sewa</th>
							
							</tr>
				<?php
					$no=1;
					$result = mysqli_query($conn, "SELECT * FROM pengembalian");
					while($data2=mysqli_fetch_assoc($result)) {
				?>
				<div class="box-body">
							<tr>
								<td style="width: 10px"><?php echo $no++?></td>
								<td><?php echo $data2['tgl_kembali']?></td>
								<td><?php echo $data2['denda']?></td>
								<td><?php echo $data2['biayatotal']?></td>
								<td><?php echo $data2['id_sewa']?></td>
							</tr>
							<?php
								}
							?>
						</table>
				</div>
				
			</div>	
		</div>
    </div>
	
	<?php
		require "footer.php";
	?>

    <script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>
</html>	