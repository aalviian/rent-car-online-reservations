<!DOCTYPE html>
<?php
	include "connect.php";
?>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Cars | Rent Car</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
	
	<?php
		require "header.php";
	?>
	
	<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="left-sidebar">
						<h2>Category</h2>
						<div class="panel-group category-products" id="accordian"><!--category-productsr-->
							<?php 
								$query1=mysqli_query($conn, "SELECT DISTINCT kategori FROM biodata_mobil");
								while ($kategori=mysqli_fetch_assoc($query1)) {
							?>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordian" href="#<?php echo $kategori['kategori'] ?>">
											<span class="badge pull-right"><i class="fa fa-plus"></i></span>
											<?php echo $kategori['kategori'] ?>
										</a>
									</h4>
								</div>
								<div id="<?php echo $kategori['kategori'] ?>" class="panel-collapse collapse">
									<div class="panel-body">
										<ul>
											<?php 
												$temp=$kategori['kategori'];
												$query2 = mysqli_query($conn,"SELECT nama_mobil, id_mobil FROM biodata_mobil WHERE kategori='$temp' " );
												while ($mobil2=mysqli_fetch_assoc($query2)){
											?>
											<li><a href="sewa.php?idmobil=<?php echo $mobil2['id_mobil']?>"> <?php echo $mobil2['nama_mobil']?> </a></li>
											<?php } ?>
										</ul>
									</div>
								</div>
							</div>
							<?php } ?>							
						</div><!--/category-products-->
					</div>
				</div>
				
				<div class="col-sm-9 padding-right">
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center">Cars Items</h2>
						
						<?php
							$qry = mysqli_query($conn,"SELECT * FROM biodata_mobil");
							while ($mobil = mysqli_fetch_assoc($qry)) {
						?>
						<div class="col-sm-4">
							<div class="product-image-wrapper">
								<div class="single-products">
										<div class="productinfo text-center">
											 <img src="<?php echo $mobil['foto']; ?>"
											<h2><?php echo"Rp ".number_format($mobil['harga'],0) ?>/hari</h2>
											<p><?php echo $mobil['nama_mobil']?></p>
													<?php
													if($mobil['stok'] <= 0) { ?>
														<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Sewa</a>
													<?php } 
													else {?>
													<a href="sewa.php?idmobil=<?php echo $mobil['id_mobil']?>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Sewa</a>
													<?php } ?>
										</div>
										<div class="product-overlay">
											<div class="overlay-content">
												<?php
													if($mobil['stok'] == 0) { ?>
														<p><h2>Full Booked</h2></p>
													<?php } else { ?>
														<p>Sedia <?php echo $mobil['stok']?> Stok</p>
													<?php }
												?>
												<h2><?php echo"Rp ".number_format($mobil['harga'],0) ?>/hari</h2>
												<p><?php echo $mobil['nama_mobil']?></p>
													<?php
													if($mobil['stok'] <= 0) { ?>
														<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Sewa</a>
													<?php } 
													else {?>
													<a href="sewa.php?idmobil=<?php echo $mobil['id_mobil']?>" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Sewa</a>
													<?php } ?>
											</div>
										</div>
								</div>
							</div>
						</div>
						<?php } ?>
					
						
						<ul class="pagination">
							<li class="active"><a href="">1</a></li>
							<li><a href="">2</a></li>
							<li><a href="">3</a></li>
							<li><a href="">&raquo;</a></li>
						</ul>
					</div><!--features_items-->
				</div>
			</div>
		</div>
	</section>
	
	<?php	
		require "footer.php";
	?>
  
    <script src="js/jquery.js"></script>
	<script src="js/price-range.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>
</html>